package ru.dexsys;

import java.util.Scanner;
import java.util.logging.Logger;

import static ru.dexsys.helpers.ArraysActionsHelper.*;

public class Main {

    //    необходимо реализовать команды
    private static final String commands = "\n" +
                    "init array - инициализация списков набором значений array\n" +
                    "print      - печать всех списков\n" +
                    "print type - печать конкретного списка, где type принимает значения X,S,M\n" +
                    "anyMore    - выводит на экран были ли значения не вошедшие ни в один список, возможные значения true, false\n" +
                    "clear type - чистка списка , где type принимает значения X,S,M\n" +
                    "merge      - слить все списки в один вывести на экран и очистить все списки\n" +
                    "exit       - выход из программы\n" +
                    "help       - вывод справки по командам";
//
//    для каждого набора значения должы быть отсортированы в порядке возрастания
//    если какой то из списков пустой необходимо напечатать сообщение "Список type пуст" при выводе списка с помощью команд Print
//    Исходный код направить в виде архива или выложить на https://bitbucket.org

    private static final Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        log.info("Введите команду или наберите 'help' для вызова справки по командам: ");

        while (true) {
            String[] command;
            Scanner in = new Scanner(System.in);

            String consoleInput = in.nextLine().trim();
            command = consoleInput.split("\\s");

            switch (command[0]) {
                case "init":
                    initArrays(command);
                    break;
                case "print":
                    printArrays(command);
                    break;
                case "anyMore":
                    notInArray();
                    break;
                case "clear":
                    clearArray(command);
                    break;
                case "merge":
                    mergeArray();
                    break;
                case "help":
                    log.info(commands);
                    break;
                case "exit":
                    System.exit(0);
                    break;
                default:
                    log.info("Некорректная команда. Наберите 'help' для помощи.");
            }
        }
    }
}
