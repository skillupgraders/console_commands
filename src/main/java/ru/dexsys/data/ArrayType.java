package ru.dexsys.data;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public enum ArrayType {

    M(new ArrayList<>()),
    S(new ArrayList<>()),
    X(new ArrayList<>());

    @Getter
    private List<Integer> list;

    ArrayType(List<Integer> list) {
        this.list = list;
    }

    @Override
    public String toString(){
        return list.isEmpty() ? "Список " + this.name() + " пуст" :
                "Список " + this.name() + ": " + list.toString();
    }

    public static void initArrays(Integer num) {
        if (num % 3 == 0) {
            X.getList().add(num);
        }
        if (num % 7 == 0) {
            S.getList().add(num);
        }
        if (num % 21 == 0) {
            M.getList().add(num);
        }
    }
}
