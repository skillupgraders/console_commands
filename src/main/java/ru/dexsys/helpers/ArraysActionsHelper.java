package ru.dexsys.helpers;

import ru.dexsys.data.ArrayType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class ArraysActionsHelper {

    private static final Logger log = Logger.getLogger(ArraysActionsHelper.class.getName());
    private static boolean notInArray = false;

    public static void initArrays(String[] command) {
        notInArray = false;
        for (int i = 1; i < command.length; i++) {
            String arg = command[i];
            try {
                Integer number = Integer.parseInt(arg);
                if (number % 3 != 0 && number % 7 != 0) {
                    log.info("Число " + arg + " не входит ни в один список.");
                    notInArray = true;
                } else {
                    ArrayType.initArrays(number);
                    log.info("Список инициализирован значением: " + arg);
                }
            } catch (NumberFormatException nfe) {
                log.info("Необходимо ввести только целые числа");
            }
        }
    }

    public static void printArrays(String[] command) {
        if (command.length == 1) {
            log.info(Arrays.toString(ArrayType.values()));
            log.info(Arrays.toString(ArrayType.values()));
        } else {
            for (int i = 1; i < command.length; i++) {
                String type = command[i].toUpperCase();
                try {
                    List<Integer> list = ArrayType.valueOf(type).getList();
                    if (list.isEmpty()) {
                        log.info("Список " + type + " пуст");
                    } else {
                        log.info("Список " + type + ": " + list.toString());
                    }
                } catch (IllegalArgumentException e) {
                    log.info("Введите тип одного из списков: " + Arrays.toString(ArrayType.values()));
                }
            }
        }
    }

    public static void notInArray(){
        log.info("" + notInArray);
    }

    public static void clearArray(String[] command) {
        if (command.length == 1) {
            log.info("Укажите тип массива: " + Arrays.toString(ArrayType.values()));
        } else {
            for (int i = 1; i < command.length; i++) {
                String type = command[i];
                ArrayType.valueOf(type).getList().clear();
            }
        }
    }

    public static void mergeArray(){
        List<Integer> listValues = new ArrayList<>();
        for (ArrayType type : ArrayType.values()) {
            listValues.addAll(type.getList());
            type.getList().clear();
            log.info("Список " + type.name() + " очищен");
        }
        log.info("Значения всех списков: " + listValues);
    }
}
